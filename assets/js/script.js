function cookiesBar() {

    // Get the bar.
    const cookieBar = document.querySelector('#cookieAcceptBar'),
        cookieBtn = document.querySelector('#cookieAcceptBarConfirm');

    // Set the default visibility.
    let isVisible = true;

    // Get the visibility from a cookie.
    let isVisibleValue = Cookies.get('staropramenIgraCookies');

    // Check if it isn't undefined
    if (isVisibleValue !== undefined || isVisibleValue === 'visible') {

        // Hide the bar and set the cookie.
        // Cookies.set('staropramenIgraCookies', 'visible', {expires: 365});
        Cookies.set('staropramenIgraCookies', 'visible', {sameSite: 'None', secure: true, expires: 365});

        isVisible = false;

        cookieBar.classList.add('none');

    }

    cookieBtn.addEventListener('click', (event) => {

        Cookies.set('staropramenIgraCookies', 'visible', {sameSite: 'None', secure: true, expires: 365});

        isVisible = false;

        cookieBar.classList.add('none');

    });

}

$(function () {
    cookiesBar();
});
