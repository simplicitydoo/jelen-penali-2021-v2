<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class EndGameService {
  private $entityManager;

  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
  }


  public function checkIfEndDate(): bool {
    if (new \DateTime($_ENV['END_GAME']) < new \DateTime()) {
      return true;
    } else {
      return false;
    }
  }

//  public function checkIfEndTopList(): bool {
//
//    $config = $this->entityManager->getRepository('App\Entity\Config')->find(1);
//
//    if ($this->checkIfEndDate() && $config->getGameFinished()) {
//      return true;
//    } else {
//      return false;
//    }
//  }

}
