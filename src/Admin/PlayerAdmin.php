<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PlayerAdmin extends AbstractAdmin {

  protected function configureListFields(ListMapper $listMapper) {
    $listMapper
      ->add('id')
      ->add('firstName')
      ->add('lastName')
      ->add('phone')
      ->add('address')
      ->add('city')
      ->add('goals')
      ->add('isRegistered')
      ->add('ageGate')
      ->add('isCheat')
      ->add('created', null, ['format' => 'd.m.Y H:i:s']);
  }

  protected function configureDatagridFilters(DatagridMapper $datagridMapper): void {
    $datagridMapper
      ->add('id')
      ->add('firstName')
      ->add('lastName')
      ->add('phone');
  }


  /**
   * @param FormMapper $formMapper
   */
  protected function configureFormFields(FormMapper $formMapper) {
    $formMapper
      ->add('firstName')
      ->add('lastName');
    ;
  }


  /**
   * {@inheritdoc}
   */
  protected function configureRoutes(RouteCollection $collection) {
    $collection->remove('batch');
    $collection->remove('export');
    $collection->remove('create');
    $collection->remove('delete');
    $collection->remove('show');
    $collection->remove('edit');
  }

}
