<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;


class CodeAdmin extends AbstractAdmin {
  /**
   * @param DatagridMapper $datagridMapper
   */
  protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
    $datagridMapper
      ->add('code', null, ['show_filter' => true])
      ->add('used')
      ->add('created')
      ->add('player.phone');
  }

  /**
   * @param ListMapper $listMapper
   */
  protected function configureListFields(ListMapper $listMapper) {
    $listMapper
      ->add('id')
      ->add('code')
      ->add('used')
      ->add('player.phone')
      ->add('isTest')
      ->add('created', null, ['format' => 'd.m.Y H:i:s']);
  }

  /**
   * @param FormMapper $formMapper
   */
  protected function configureFormFields(FormMapper $formMapper) {
    $formMapper
      ->add('id')
      ->add('code')
      ->add('used')
      ->add('created')
    ;
  }

  /**
   * @param ShowMapper $showMapper
   */
  protected function configureShowFields(ShowMapper $showMapper) {
    $showMapper
      ->add('id')
      ->add('code')
      ->add('used')
      ->add('created')
    ;
  }


  /**
   * {@inheritdoc}
   */
  protected function configureRoutes(RouteCollection $collection) {
    $collection->remove('batch');
    $collection->remove('export');
    $collection->remove('create');
    $collection->remove('delete');
    $collection->remove('show');
    $collection->remove('edit');

  }

}
