<?php

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {

  private $em;

  public function __construct(EntityManagerInterface $em) {
    $this->em = $em;
  }

  public function getFilters() {
    return [
      new TwigFilter('makeTime', array($this, 'makeTime'))
    ];
  }

  public function getFunctions() {
    return array(
      new TwigFunction('getenv', [$this, 'getenv']),
    );
  }

  public function makeTime(?int $time): ?string {

    if (is_null($time)) {
      return null;
    }

    $uSec = $time % 1000;
    $uSec /= 10;
    $time = floor($time / 1000);

    $seconds = $time % 60;
    if ($seconds < 10) {
      $seconds = '0' . $seconds;
    }

    $time = floor($time / 60);

    $minutes = $time % 60;
    if ($minutes < 10) {
      $minutes = '0' . $minutes;
    }

    return $minutes . ':' . $seconds . '.' . $uSec;
  }

  public function getenv(string $name, string $type = '') {
    if ($type == 'int') {
      return (int)$_ENV[$name];
    }
    if ($type == 'float') {
      return (float)$_ENV[$name];
    }

    return $_ENV[$name];
  }

}
