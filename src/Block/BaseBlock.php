<?php

namespace App\Block;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BaseBlock extends AbstractBlockService {

  protected $templating;

  /**
   * BaseBlock constructor.
   * @param Environment $templating
   */
  public function __construct($name, Environment $templating) {
    parent::__construct($templating);

    $this->templating = $templating;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'Export';
  }

  /**
   * {@inheritdoc}
   */
  public function configureSettings(OptionsResolver $resolver) : void {
    $resolver->setDefaults(array(
      'url' => false,
      'title' => 'Export',
      'template' => 'Block/base.html.twig',
    ));
  }

  public function execute(BlockContextInterface $blockContext, Response $response = null) : Response {
    $settings = $blockContext->getSettings();

    return $this->renderResponse('Block/base.html.twig', array(
      'block' => $blockContext->getBlock(),
      'settings' => $settings
    ), $response);
  }

  /**
   * Returns a Response object than can be cacheable.
   * @param string $view
   * @return Response
   * @throws
   */
  public function renderResponse($view, array $parameters = [], Response $response = null) : Response
  {

    $response = new Response();
    $response->setContent($this->templating->render($view, $parameters));

    return $response;
  }

}
