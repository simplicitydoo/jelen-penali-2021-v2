<?php

namespace App\Controller;

use App\Entity\Player;
use App\Service\EndGameService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController {

  /**
   * @Route("/game-intro", name="game_intro")
   * @param EndGameService $endGameService
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function gameIntro(EndGameService $endGameService, Session $session) {
    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    if ($endGameService->checkIfEndDate()) {
      return $this->redirectToRoute('top_lista');
    }

    $player = $this->getDoctrine()->getRepository(Player::class)->find($session->get('playerId'));

    if (!$player->getIsRegistered()) {
      return $this->redirectToRoute('player_register');
    }

    $args = [];

    return $this->render('game/game-intro.html.twig', $args);
  }

//  /**
//   * @Route("/game-rules", name="game_rules")
//   * @param EndGameService $endGameService
//   * @param Session $session
//   * @return RedirectResponse|Response
//   */
//  public function gameRules(EndGameService $endGameService, Session $session) {
//    if (!($session->get('ageGate'))) {
//      return $this->redirectToRoute('age_gate');
//    }
//
//    if ($endGameService->checkIfEndDate()) {
//      return $this->redirectToRoute('top_lista');
//    }
//
//    $args = [];
//
//    return $this->render('game/game-rules.html.twig', $args);
//  }

  /**
   * @Route("/game", name="game")
   * @param EndGameService $endGameService
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function game(EndGameService $endGameService, Session $session) {
    $args = [];
    $args['code'] = '';

    if(!($session->get('code'))){
      return $this->redirectToRoute('age_gate');
    }

    if ($endGameService->checkIfEndDate()) {
      return $this->redirectToRoute('top_lista');
    }

    if (!$session->has('gameId')){
      return $this->redirectToRoute('age_gate');
    }

    $gameId = $session->get('gameId');
    $game = $this->getDoctrine()->getRepository('App:Game')->find($gameId);

    if(!$game) {
      return $this->redirectToRoute('age_gate');
    }

    if ($game->getServerStartTime() != null) {
      $session->remove('gameId');
      $session->remove('code');

      return $this->redirectToRoute('age_gate');
    }

    $args['code'] = $game->getHashCode();
    $args['playerId'] = $session->get('playerId');

    $response = $this->render('game/index.html.twig', $args);

    $response->headers->set("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    $response->headers->set("Pragma", "no-cache"); // HTTP 1.0.
    $response->headers->set("Expires", "0"); // Proxies.

    return $response;
  }

  /**
   * @Route("/placement/{id}", name="placement")
   * @param Player $player
   * @return JsonResponse
   */
  public function placement(Player $player): JsonResponse {
    $args = [];

    $args['placement'] = $this->getDoctrine()->getRepository('App:Player')->getPlace($player);

    return new JsonResponse($args);
  }
}
