<?php

namespace App\Controller;

use App\Entity\Code;
use App\Entity\Game;
use App\Entity\Player;
use App\Service\EndGameService;
use Exception;
use Firebase\JWT\JWT;
use Hybridauth\Hybridauth;
use Hybridauth\Provider\Facebook;
use Hybridauth\Provider\Google;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController {

  /**
   * @Route("/age", name="age_gate")
   * @param Request $request
   * @param Session $session
   * @return Response
   * @throws Exception
   */
  public function ageGate(Request $request, Session $session): Response {

    $args = [];

    if ($session->get('ageGate')) {
      return $this->redirectToRoute('code_register');
    }

    if ($request->getMethod() == "POST") {

      $year = $request->request->getInt('year');
      $month = $request->request->getInt('month');
      $day = $request->request->getInt('day');

      if ($year == 0 || $month == 0 || $day == 0) {
        return $this->redirectToRoute('age_gate');
      }
      $birthDay = new \DateTime($year . '-' . $month . '-' . $day);
      $now = new \DateTime();
      $interval = $now->diff($birthDay);

      if ($interval->y < 18) {
        return $this->redirectToRoute('age_gate');
      }
      $player = $this->getDoctrine()->getRepository('App:Player')->create($birthDay);

      $session->set('playerId', $player->getId());
      $session->set('ageGate', true);
      $session->set('birthDay', $birthDay);

      return $this->redirectToRoute('intro');
    }

    $args['years'] = range(2021, 1939);
    $args['months'] = range(1, 12);
    $args['days'] = range(1, 31);

    return $this->render('register/age_gate.html.twig', $args);
  }

  /**
   * @Route("/code-register", name="code_register")
   * @param EndGameService $endGameService
   * @param Request $request
   * @param Session $session
   * @return Response
   */
  public function codeRegister(EndGameService $endGameService, Request $request, Session $session): Response {

    if (!$session->has('ageGate')) {
      return $this->redirectToRoute('age_gate');
    }

    if ($endGameService->checkIfEndDate()) {
      return $this->redirectToRoute('top_lista');
    }

    if ($request->getMethod() == "POST") {
      $code = $request->request->get('code');

      $check = $this->getDoctrine()->getRepository('App:Code')->checkCode($code);
      $args['check'] = $check;
      $playerId = $session->get('playerId');
      $codeEntity = $this->getDoctrine()->getRepository('App:Code')->save($code, $playerId, $check);
      $session->set('codeId', $codeEntity->getId());
      $accept = $request->request->getInt('accept') === 1;
      if (!$accept) {
        return $this->redirectToRoute('code_register');
      }

      if ($check == 1) {
        $game = $this->getDoctrine()->getRepository('App:Game')->create($codeEntity, $playerId);
        $session->set('gameId', $game->getId());
        $session->set('code', $code);
        $session->set('accept', 1);

        if ($session->get('isRegistered') && $check) {

          return $this->redirectToRoute('game');
        }
        return $this->redirectToRoute('player_register');

      } else if ($check == 0){
        $this->addFlash('codeNotValid', true);
        return $this->redirectToRoute('code_register');
      } else if ($check == -1) {
        $this->addFlash('codeUsed', true);
        return $this->redirectToRoute('code_register');
      }
    }

    return $this->render('register/code.html.twig');
  }

  /**
   * @Route("/register", name="player_register")
   * @param EndGameService $endGameService
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function playerRegister(EndGameService $endGameService, Request $request, Session $session) {

    if ($session->get('ageGate') != 1) {
      return $this->redirectToRoute('age_gate');
    } elseif ($session->get('accept') != 1 || !$session->has('code')) {
      return $this->redirectToRoute('code_register');
    }

    if ($endGameService->checkIfEndDate()) {
      return $this->redirectToRoute('top_lista');
    }

    $playerData = [];

    if ($request->getMethod() == "POST") {

      $playerData['firstName'] = $request->request->get('firstName');
      $playerData['lastName'] = $request->request->get('lastName');
      $playerData['phone'] = $request->request->get('phone');
      $playerData['city'] = $request->request->get('city');
      $playerData['address'] = $request->request->get('address');
      $playerData['email'] = $request->request->get('email');
      $playerData['birthday'] = $session->get('birthDay');
      $playerData['ageGate'] = $session->get('ageGate');

      $phone = $res = preg_replace("/[^0-9]/", "", $playerData['phone']);
      $player = $this->getDoctrine()->getRepository(Player::class)->findOneBy(['phone' => $phone, 'email' => $playerData['email']]);

      if (is_null($player)) {
        $playerId = $session->get('playerId');

        $player = $this->getDoctrine()->getRepository(Player::class)->addData($playerData, $playerId);
      }
      $codeId = $session->get('codeId');

      if (!is_null($codeId)) {
        $this->getDoctrine()->getRepository(Code::class)->addPlayer($player, $codeId);
      }

      if ($session->has('gameId')) {
        $gameId = $session->get('gameId');
        $this->getDoctrine()->getRepository(Game::class)->addPlayer($gameId, $player->getId());
      }

      $session->set('playerId', $player->getId());
      $session->set('isRegistered', $player->getIsRegistered());

      return $this->redirectToRoute('game_intro');
    }

    return $this->render('register/player.html.twig');
  }

  /**
   * @Route("/player-auth", options = { "expose" = true }, name="player_auth")
   * @param Session $session
   * @return JsonResponse
   */
  public function playerAuth(Session $session): JsonResponse {

    $playerId = $session->has('playerId') ? $session->get('playerId') : null;
    $date = new \DateTimeImmutable();

    $secretKey  = $_ENV['SECRET_KEY'];
    $issuedAt   = $date->getTimestamp();
    $expire     = $date->modify('+365 days')->getTimestamp();
    $serverName = $_SERVER['HTTP_HOST'];

    $data = [
      'iat'  => $issuedAt,         // Issued at: time when the token was generated
      'iss'  => $serverName,       // Issuer
      'nbf'  => $issuedAt,         // Not before
      'exp'  => $expire,           // Expire
      'playerId' => $playerId,
    ];

    $token = JWT::encode($data, $secretKey, 'HS512');

    return new JsonResponse(['token' => $token]);
  }

  /**
   * @Route("/facebook/login/", name="fb_login")
   * @param Session $session
   * @return Response|null
   */
  public function facebookLogin(Session $session): ?Response {

    $config = array(
      "callback" => $_ENV['BASE_URL'] . '/facebook/login/',
      "keys"     => array("id" => $_ENV['FB_APP_ID'], "secret" => $_ENV['FB_APP_SECRET']),
    );

    $fb = new Facebook( $config );
    try {
      $fb->authenticate();
      $user_profile = $fb->getUserProfile();
    } catch (Exception $e) {
      return null;
    }

    $user = [
//      'fb_id' => $user_profile->identifier,
//      'name'  => $user_profile->displayName,
      'fname' => $user_profile->firstName,
      'lname' => $user_profile->lastName,
      'email' => !empty($user_profile->email) ? $user_profile->email : ''
    ];

    if ($user) {
      $player = $this->getDoctrine()->getRepository(Player::class)->findOneBy(['email' => $user['email']]);

      if (!$player) {
        $playerId = $session->get('playerId');

        $player = $this->getDoctrine()->getRepository(Player::class)->addFbData($user, $playerId);
      }

      $session->set('playerId', $player->getId());

      $codeId = $session->get('codeId');

      if (!is_null($codeId)) {
        $this->getDoctrine()->getRepository(Code::class)->addPlayer($player, $codeId);
      }

      if ($session->has('gameId')) {
        $gameId = $session->get('gameId');
        $this->getDoctrine()->getRepository(Game::class)->addPlayer($gameId, $player->getId());
      }

      if (!empty($player->getPhone())) {
        $args['redirectUrl'] = $this->generateUrl('game_intro');
        return $this->render('register/close_window.html.twig', $args);
      }

      $args['redirectUrl'] = $this->generateUrl('add_phone');
      return $this->render('register/close_window.html.twig', $args);
    }

  }

  /**
   * @Route("/google/login/", name="google_login")
   * @param Session $session
   * @return Response
   */
  public function googleLogin(Session $session): Response {

    $config = array(
      "callback" => $_ENV['BASE_URL'] . '/google/login/',
      "keys"     => array("id" => $_ENV['GOOGLE_CLIENT_ID'], "secret" => $_ENV['GOOGLE_CLIENT_SECRET']),
    );

    $google = new Google( $config );
    try {
      $google->authenticate();
      $user_profile = $google->getUserProfile();
    } catch (Exception $e) {
      dump($e->getMessage());
      die();
    }

    $user = [
      'fname' => $user_profile->firstName,
      'lname' => $user_profile->lastName,
      'email' => $user_profile->email
    ];

    if ($user) {
      $player = $this->getDoctrine()->getRepository(Player::class)->findOneBy(['email' => $user['email']]);

      if (!$player) {
        $playerId = $session->get('playerId');

        $player = $this->getDoctrine()->getRepository(Player::class)->addGoogleData($user, $playerId);
      }

      $session->set('playerId', $player->getId());

      $codeId = $session->get('codeId');

      if (!is_null($codeId)) {
        $this->getDoctrine()->getRepository(Code::class)->addPlayer($player, $codeId);
      }

      if ($session->has('gameId')) {
        $gameId = $session->get('gameId');
        $this->getDoctrine()->getRepository(Game::class)->addPlayer($gameId, $player->getId());
      }

      if (!empty($player->getPhone())) {
        $args['redirectUrl'] = $this->generateUrl('game_intro');
        return $this->render('register/close_window.html.twig', $args);
      }

      $args['redirectUrl'] = $this->generateUrl('add_phone');
      return $this->render('register/close_window.html.twig', $args);
    }
  }

  /**
   * @Route("/add-phone", name="add_phone")
   * @param EndGameService $endGameService
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function playerPhone(Request $request, EndGameService $endGameService, Session $session) {
    if (!$session->has('ageGate')) {
      return $this->redirectToRoute('age_gate');
    }

    if ($endGameService->checkIfEndDate()) {
      return $this->redirectToRoute('top_lista');
    }

    if (!$session->has('playerId')) {
      return $this->redirectToRoute('age_gate');
    }

    $player = $this->getDoctrine()->getRepository(Player::class)->find($session->get('playerId'));

    if (!$player->getFbLogin() && !$player->getGoogleLogin()){
      return $this->redirectToRoute('game_intro');
    }

    if ($request->getMethod() == 'POST') {
      $phone = $res = preg_replace("/[^0-9]/", "", $request->request->get('phone'));

      $this->getDoctrine()->getRepository(Player::class)->addPhone($player, $phone);

      return $this->redirectToRoute('game_intro');
    }


    return $this->render('register/add_phone.html.twig');

  }
}
