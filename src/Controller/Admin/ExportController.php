<?php

namespace App\Controller\Admin;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\PlayerGoal;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class ExportController extends AbstractController {

  /**
   * @Route("/export-players", name="export_players")
   * @return StreamedResponse
   * @throws \Exception
   */
  function exportPlayers() {

    if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
      return $this->redirect('/admin/dashboard');
    }

//
    set_time_limit(300);
//
//    $players = $this->getDoctrine()->getRepository(Player::class)->checkTimes();
//
//    foreach ($players as $player) {
//      $d1 = $player['serverStartTime'];
//      $d2 = $player['serverEndTime'];
//
//      if (is_null($d1) || is_null($d2)) {
////        $this->getDoctrine()->getRepository(Player::class)->setCheater($player['playerId'], 'Vreme');
//        continue;
//      }
//
//      $interval = $d1->diff($d2);
//      $secondDiff = $interval->s;
//      $player['time'] = $secondDiff;
//
//      if ($interval->i > 0) {
//        $player['time'] += $interval->i * 60;
//      }
//
//      $this->getDoctrine()->getRepository(Player::class)->timeForAllGames($player['playerId'], $player['time']);
//    }
//
//      if ($player['time'] < 30) {
//        $this->getDoctrine()->getRepository(Player::class)->setCheater($player['playerId'], 'Vreme');
//      } else {
//        $this->getDoctrine()->getRepository(Player::class)->setValid($player['playerId']);
//      }
//    }
//

//    $cheaters = $this->getDoctrine()->getRepository(Player::class)->getKickCheaters();
//
//    foreach ($cheaters as $cheater) {
//      $this->getDoctrine()->getRepository(Game::class)->setInvalid($cheater['gameId']);
//    }

//
//    $cheaters = $this->getDoctrine()->getRepository(Player::class)->getAllCheaters();

//    $cheaters = $this->getDoctrine()->getRepository(Player::class)->findBy(['isCheat' => true, 'goals' => 0]);
//
//
//    foreach ($cheaters as $cheater) {
//      $this->getDoctrine()->getRepository(Game::class)->calculateGames($cheater->getId());
//    }

//    dump('done');
//    die();


    $players = $this->getDoctrine()->getRepository(Player::class)->getPlayersExport();

//    foreach ($players as $player) {
//      $this->getDoctrine()->getRepository(Player::class)->backFromTheDead($player['playerId'], $player['num_of_goals']);
//    }
//    dump($players);
//    die();

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getProperties()->setCreator('Jelen')
      ->setTitle('Igraci');
    $titles = ['pozicija', 'ime', 'prezime', 'adresa', 'telefon', 'email', 'grad', 'broj_golova', 'prosecan_broj_golova', 'broj_igara', 'cheater', 'razlog', 'zero_player'];
    $fields = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];

    try {
      $spreadsheet->setActiveSheetIndex(0);
    } catch (Exception $e) {
    }
    for ($i = 0; $i < count($titles); $i++) {
      try {
        $spreadsheet->getActiveSheet()->setCellValue($fields[$i] . '1', $titles[$i]);
      } catch (Exception $e) {
      }
    }
    $i = 2;
    foreach ($players as $key => $value) {
      try {
        $spreadsheet->getActiveSheet()
          ->setCellValue($fields[0] . $i, $i - 1)
          ->setCellValue($fields[1] . $i, $value['firstName'])
          ->setCellValue($fields[2] . $i, $value['lastName'])
          ->setCellValue($fields[3] . $i, $value['address'])
          ->setCellValue($fields[4] . $i, $value['phone'])
          ->setCellValue($fields[5] . $i, $value['email'])
          ->setCellValue($fields[6] . $i, $value['city'])
          ->setCellValue($fields[7] . $i, $value['goals'])
          ->setCellValue($fields[8] . $i, $value['avgGoals'])
          ->setCellValue($fields[9] . $i, $value['played_games'])
          ->setCellValue($fields[10] . $i, $value['isCheat'])
          ->setCellValue($fields[11] . $i, !is_null($value['banReason']) ? $value['banReason'] : '')
          ->setCellValue($fields[12] . $i, !is_null($value['zeroPlayer']) ? $value['zeroPlayer'] : '');

      } catch (Exception $e) {
      }
      $i++;
    }
    foreach ($fields as $key => $value) {
      try {
        $spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
      } catch (Exception $e) {
      }
    }

    try {
      $spreadsheet->getActiveSheet()->setTitle('Svi igraci')
        ->getPageSetup()
        ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
        ->setFitToPage(true);
    } catch (Exception $e) {
    }
    $writer = new Xls($spreadsheet);

    $response = new StreamedResponse(
      function () use ($writer) {
        $writer->save('php://output');
      }
    );

    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'Lista_igraca_' . (new \DateTime())->format('Ymd') . '.xls'
    );

    $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response;
  }

  /**
   * @Route("/export-cheaters", name="export_cheaters")
   * @return StreamedResponse
   * @throws \Exception
   */
  function exportCheaters() {

    if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
      return $this->redirect('/admin/dashboard');
    }

    $players = $this->getDoctrine()->getRepository(Player::class)->getAllCheaters();

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getProperties()->setCreator('Jelen')
      ->setTitle('Igraci');
    $titles = ['ime', 'prezime', 'adresa', 'telefon', 'email', 'grad', 'razlog'];
    $fields = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

    try {
      $spreadsheet->setActiveSheetIndex(0);
    } catch (Exception $e) {
    }
    for ($i = 0; $i < count($titles); $i++) {
      try {
        $spreadsheet->getActiveSheet()->setCellValue($fields[$i] . '1', $titles[$i]);
      } catch (Exception $e) {
      }
    }
    $i = 2;
    foreach ($players as $key => $value) {
      try {
        $spreadsheet->getActiveSheet()
          ->setCellValue($fields[0] . $i, $value['firstName'])
          ->setCellValue($fields[1] . $i, $value['lastName'])
          ->setCellValue($fields[2] . $i, $value['address'])
          ->setCellValue($fields[3] . $i, $value['phone'])
          ->setCellValue($fields[4] . $i, $value['email'])
          ->setCellValue($fields[5] . $i, $value['city'])
          ->setCellValue($fields[6] . $i, $value['banReason']);
      } catch (Exception $e) {
      }
      $i++;
    }
    foreach ($fields as $key => $value) {
      try {
        $spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
      } catch (Exception $e) {
      }
    }

    try {
      $spreadsheet->getActiveSheet()->setTitle('Svi igraci')
        ->getPageSetup()
        ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
        ->setFitToPage(true);
    } catch (Exception $e) {
    }
    $writer = new Xls($spreadsheet);

    $response = new StreamedResponse(
      function () use ($writer) {
        $writer->save('php://output');
      }
    );

    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'Cheaters_' . (new \DateTime())->format('Ymd') . '.xls'
    );

    $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response;
  }
}
