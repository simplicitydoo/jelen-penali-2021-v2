<?php

namespace App\Controller;

use App\Classes\AppConfig;
use App\Entity\Player;
use App\Entity\PlayerGoal;
use App\Service\EndGameService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {

  /**
   * @Route("/home", name="home")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function index(Session $session) {
    $session->set('function', __FUNCTION__);


    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    $args = [];

    return $this->render('default/home.html.twig', $args);
  }

  /**
   * @Route("/", name="intro")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function intro(Session $session) {
    $session->set('function', __FUNCTION__);

    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    $args = [];

    return $this->render('default/intro.html.twig', $args);
  }

  /**
   * @Route("/nagrade", name="nagrade")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function nagrade(Session $session) {
    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    $args = [];

    return $this->render('default/nagrade.html.twig', $args);
  }

  /**
   * @Route("/pravila", name="pravila")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function pravila(Session $session) {
    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }
    $args = [];

    return $this->render('default/pravila.html.twig', $args);
  }

  /**
   * @Route("/politika-privatnosti/", name="politika-privatnosti")
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function politikaPrivatnosti(Session $session) {
    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    $args = [];

    return $this->render('default/politika-privatnosti.html.twig', $args);
  }

  /**
   * @Route("/top-lista", name="top_lista")
   * @param EndGameService $endGameService
   * @param PaginatorInterface $paginator
   * @param Request $request
   * @param Session $session
   * @return RedirectResponse|Response
   */
  public function topList(EndGameService $endGameService, PaginatorInterface $paginator, Request $request, Session $session) {
    if (!($session->get('ageGate'))) {
      return $this->redirectToRoute('age_gate');
    }

    if ($session->has('gameId')) {
      $numOfKicks= $this->getDoctrine()->getRepository(PlayerGoal::class)->checkKicks($session->get('playerId'), $session->get('gameId'));

      if ($numOfKicks > 15) {
        $this->getDoctrine()->getRepository(Player::class)->setCheater($session->get('playerId'), 'Broj šuteva');
      }

      $this->getDoctrine()->getRepository(Player::class)->addGameTime($session->get('playerId'), $session->get('gameId'));

      $session->remove('gameId');
      $session->remove('code');
      $session->remove('codeId');
    }

    if ($session->has('playerId')) {
      $playerId = $session->get('playerId');

      $this->getDoctrine()->getRepository(Player::class)->saveAverageGoals($playerId);
    }

    $args = [];

    $players = $this->getDoctrine()
      ->getRepository(Player::class)
      ->getByGoals();

    $pagination = $paginator->paginate(
      $players, /* query NOT result */
      $request->query->getInt('page', 1), /*page number*/
      AppConfig::ITEMS_LIMIT /*limit per page*/
    );

//    $pagination->setTotalItemCount(1660);

    $args['pagination'] = $pagination;
    $args['itemPerPage'] = AppConfig::ITEMS_LIMIT;

    $args['endDate'] = $endGameService->checkIfEndDate();

    $response = $this->render('default/top-lista.html.twig', $args);

    $response->headers->set("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    $response->headers->set("Pragma", "no-cache"); // HTTP 1.0.
    $response->headers->set("Expires", "0"); // Proxies.

    return $response;
  }

}
