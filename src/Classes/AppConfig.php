<?php


namespace App\Classes;


class AppConfig {

  public const ITEMS_LIMIT = 10;
  public const SUCCESS = 0;
  public const ERROR_NOT_GRANTED = -1;
  public const ERROR_NO_OBJECT = -2;
  public const ERROR_NOT_DELETED = -4;

}