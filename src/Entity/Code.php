<?php

namespace App\Entity;

use App\Repository\CodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="codes", indexes={@ORM\Index(name="code", columns={"code"})})
 * @ORM\Entity(repositoryClass=CodeRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Code {
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   */
  private $player;

  /**
   * @ORM\Column(name="code", type="string", length=255)
   */
  private $code;

  /**
   * @ORM\Column(name="used", type="boolean")
   */
  private $used;

  /**
   * @ORM\Column(name="is_test", type="boolean")
   */
  private $isTest;

  /**
   * @ORM\Column(name="verfified", type="boolean")
   */
  private $verified;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getCode(): ?string {
    return $this->code;
  }

  public function setCode(string $code): self {
    $this->code = $code;

    return $this;
  }

  public function getUsed(): ?bool {
    return $this->used;
  }

  public function setUsed(bool $used): self {
    $this->used = $used;

    return $this;
  }

  public function getIsTest(): ?bool {
    return $this->isTest;
  }

  public function setIsTest(bool $isTest): self {
    $this->isTest = $isTest;

    return $this;
  }

  public function getVerified(): ?bool {
    return $this->verified;
  }

  public function setVerified(bool $verified): self {
    $this->verified = $verified;

    return $this;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(?\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }
}
