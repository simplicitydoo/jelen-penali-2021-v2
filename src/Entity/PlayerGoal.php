<?php

namespace App\Entity;

use App\Repository\PlayerGoalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="player_goals")
 * @ORM\Entity(repositoryClass=PlayerGoalRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class PlayerGoal {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   **/
  private $player;

  /**
   * @ORM\ManyToOne(targetEntity="Game")
   * @ORM\JoinColumn(name="fk_game_id", referencedColumnName="id")
   **/
  private $game;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $goal;

  /**
   * @ORM\Column(name="token", type="boolean", nullable=true)
   */
  private $token;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getGame(): ?Game {
    return $this->game;
  }

  public function setGame(Game $game): self {
    $this->game = $game;

    return $this;
  }

  public function getGoal(): ?bool {
    return $this->goal;
  }

  public function setGoal(?bool $goal): self {
    $this->goal = $goal;

    return $this;
  }

  public function getToken(): bool {
    return $this->token;
  }

  public function setToken(bool $token): void {
    $this->token = $token;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(?\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }
}
