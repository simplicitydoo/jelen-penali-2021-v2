<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, \Serializable {
  const DEFAULT_ROLE = 'ROLE_USER';
  const ADMIN_ROLE = 'ROLE_SUPER_ADMIN';

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", unique=true)
   */
  private $email;


  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;

  /**
   * @var string
   *
   * @ORM\Column(name="password", type="string", nullable=true)
   */
  private $password;

  /**
   * A non-persisted field that's used to create the encoded password.
   * @Assert\Length(
   *      min = 6,
   *      minMessage = "Your password must be at least {{ limit }} characters long.",
   * )
   * @var string
   */
  private $plainPassword;

  /**
   * @ORM\Column(name="roles", type="text")
   */
  private $roles = self::DEFAULT_ROLE;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="last_login", type="datetime", nullable=true)
   */
  private $lastLogin;

  /**
   * @var boolean
   *
   * @ORM\Column(name="is_suspended", type="boolean")
   */
  private $isSuspended = false;

  function __toString() {
    return $this->email;
  }

  /**
   *
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  public function getId() {
    return $this->id;
  }

  public function getRoles() {
    return $this->roles;
  }

  public function getPassword() {
    return $this->password;
  }

  public function getSalt() {
  }

  public function getUsername() {
    return $this->email;
  }

  public function eraseCredentials() {
    $this->plainPassword = null;

    return $this;
  }

  public function setPlainPassword(string $plainPassword = null) {
    $this->plainPassword = $plainPassword;
    $this->password = null;

    return $this;
  }

  public function setPassword(string $password) {
    $this->password = $password;

    return $this;
  }

  public function getPlainPassword() {
    return $this->plainPassword;
  }

  public function setRoles($roles) {
    $this->roles = [];

    if (gettype($roles) == "string") {
      $this->roles = $roles;
    } else {
      foreach ($roles as $role) {
        $this->addRole($role);
      }
    }

    return $this;
  }

  public function addRole($role) {
    if (!$role) {
      return $this;
    }

    $role = strtoupper($role);
    $this->roles = [$role];
    if (!in_array($role, $this->roles, true)) {
      $this->roles[] = $role;
    }
    return $this;
  }

  public function removeRole($role) {
    if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
      unset($this->roles[$key]);
      $this->roles = array_values($this->roles);
    }

    return $this;
  }

  public function hasRole($role) {
    if ($this->roles == self::ADMIN_ROLE) {
      return false;
    }
    $this->roles = [$role];
    return in_array(strtoupper($role), $this->roles, true);
  }

  public function isAdmin($role) {
    if ($role == self::ADMIN_ROLE) {
      $this->roles = [$role];
    }
    return true;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail(string $email) {
    $this->email = $email;

    return $this;
  }

  public function getLastLogin() {
    return $this->lastLogin;
  }

  public function setLastLogin(\DateTime $lastLogin) {
    $this->lastLogin = $lastLogin;

    return $this;
  }

  public function isSuspended() {
    return $this->isSuspended;
  }

  public function setIsSuspended(bool $isSuspended) {
    $this->isSuspended = $isSuspended;

    return $this;
  }

  public function serialize() {
    return serialize(array(
      $this->id,
      $this->email,
      $this->password,
      $this->isSuspended
    ));
  }

  public function unserialize($serialized) {
    list (
      $this->id,
      $this->email,
      $this->password,
      $this->isSuspended
      ) = unserialize($serialized);
  }

  public function getIsSuspended(): ?bool {
    return $this->isSuspended;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }


}
