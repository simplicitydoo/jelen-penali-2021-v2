<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="games")
 * @ORM\Entity(repositoryClass=GameRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Game {
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Code")
   * @ORM\JoinColumn(name="fk_code_id", referencedColumnName="id")
   **/
  private $code;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @ORM\JoinColumn(name="fk_player_id", referencedColumnName="id")
   */
  private $player;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $score;

  /**
   * @ORM\Column(name="goals", type="integer", nullable=true)
   */
  private $goals;

  /**
   * @ORM\Column(name="code_entered", type="datetime", nullable=true)
   */
  private $codeEntered;

  /**
   * @ORM\Column(name="hash_code", type="string", length=255)
   */
  private $hashCode;

  /**
   * @ORM\Column(name="token", type="boolean", nullable=true)
   */
  private $token;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\Column(name="server_start_time", type="datetime", nullable=true)
   */
  private $serverStartTime;

  /**
   * @ORM\Column(name="server_end_time", type="datetime", nullable=true)
   */
  private $serverEndTime;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $isValid;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getCode(): ?Code {
    return $this->code;
  }

  public function setCode(Code $code): self {
    $this->code = $code;

    return $this;
  }

  public function getPlayer(): ?Player {
    return $this->player;
  }

  public function setPlayer(?Player $player): self {
    $this->player = $player;

    return $this;
  }

  public function getScore(): ?int {
    return $this->score;
  }

  public function setScore(?int $score): self {
    $this->score = $score;

    return $this;
  }

  public function getGoals(): ?int {
    return $this->goals;
  }

  public function setGoals(?int $goals): self {
    $this->goals = $goals;

    return $this;
  }

  public function getCodeEntered(): ?\DateTimeInterface {
    return $this->codeEntered;
  }

  public function setCodeEntered(\DateTimeInterface $codeEntered): self {
    $this->codeEntered = $codeEntered;

    return $this;
  }

  public function getHashCode(): ?string {
    return $this->hashCode;
  }

  public function setHashCode(string $hashCode): self {
    $this->hashCode = $hashCode;

    return $this;
  }

  public function getToken(): bool {
    return $this->token;
  }

  public function setToken(bool $token): void {
    $this->token = $token;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getServerStartTime(): ?\DateTimeInterface {
    return $this->serverStartTime;
  }

  public function setServerStartTime(?\DateTimeInterface $serverStartTime): self {
    $this->serverStartTime = $serverStartTime;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }

  public function getServerEndTime(): ?\DateTimeInterface {
    return $this->serverEndTime;
  }

  public function setServerEndTime(?\DateTimeInterface $serverEndTime): self {
    $this->serverEndTime = $serverEndTime;

    return $this;
  }

  public function getIsValid(): ?bool {
    return $this->isValid;
  }

  public function setIsValid(?bool $isValid): self {
    $this->isValid = $isValid;

    return $this;
  }
}
