<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="players", indexes={@ORM\Index(name="phone", columns={"phone"})})
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Player {
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(name="first_name", type="string", length=50, nullable=true)
   */
  private $firstName;

  /**
   * @ORM\Column(name="last_name", type="string", length=50, nullable=true)
   */
  private $lastName;

  /**
   * @ORM\Column(name="phone", type="string", length=50, nullable=true)
   */
  private $phone;

  /**
   * @ORM\Column(name="city", type="string", length=128, nullable=true)
   */
  private $city;

  /**
   * @ORM\Column(name="address", type="string", length=255, nullable=true)
   */
  private $address;

  /**
   * @ORM\Column(name="email", type="string", length=255, nullable=true)
   */
  private $email;

  /**
   * @ORM\ManyToOne(targetEntity="Code")
   * @ORM\JoinColumn(name="fk_code_id", referencedColumnName="id")
   **/
  private $code;

  /**
   * @ORM\Column(name="goals", type="integer", nullable=true)
   */
  private $goals;

  /**
   * @ORM\Column(name="avg_goals", type="float", nullable=true)
   */
  private $avgGoals;

  /**
   * @ORM\Column(name="age_gate", type="boolean")
   */
  private $ageGate = false;

  /**
   * @ORM\Column(name="is_registered", type="boolean", nullable=true)
   */
  private $isRegistered;

  /**
   * @ORM\Column(type="date", nullable=true)
   */
  private $dateOfBirth;

  /**
   * @ORM\Column(name="is_cheat", type="boolean", nullable=true)
   */
  private $isCheat = false;

  /**
   * @ORM\Column(name="token", type="boolean", nullable=true)
   */
  private $token;

  /**
   * @ORM\Column(name="fb_login", type="boolean", nullable=true)
   */
  private $fbLogin = false;

  /**
   * @ORM\Column(name="google_login", type="boolean", nullable=true)
   */
  private $googleLogin = false;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime", nullable=true)
   */
  private $updated;

  /**
   * @ORM\Column(name="ban_reason", type="string", length=255, nullable=true)
   */
  private $banReason;

  /**
   * @ORM\Column(name="time", type="integer", nullable=true)
   */
  private $time;

  /**
   * @ORM\Column(name="zero_player", type="boolean", nullable=true)
   */
  private $zeroPlayer;

  /**
   * @ORM\PrePersist
   */
  public function prePersist() {
    $this->created = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function preUpdate() {
    $this->updated = new \DateTime();
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getFirstName(): ?string {
    return $this->firstName;
  }

  public function setFirstName(string $firstName): self {
    $this->firstName = $firstName;

    return $this;
  }

  public function getLastName(): ?string {
    return $this->lastName;
  }

  public function setLastName(string $lastName): self {
    $this->lastName = $lastName;

    return $this;
  }

  public function getPhone(): ?string {
    return $this->phone;
  }

  public function setPhone(string $phone): self {
    $this->phone = $phone;

    return $this;
  }

  public function getCity(): ?string {
    return $this->city;
  }

  public function setCity(string $city): self {
    $this->city = $city;

    return $this;
  }

  public function getAddress(): ?string {
    return $this->address;
  }

  public function setAddress(string $address): self {
    $this->address = $address;

    return $this;
  }

  public function getEmail(): ?string {
    return $this->email;
  }

  public function setEmail(string $email): self {
    $this->email = $email;

    return $this;
  }

  public function getCode(): ?Code {
    return $this->code;
  }

  public function setCode(Code $code): self {
    $this->code = $code;

    return $this;
  }

  public function getGoals(): ?int {
    return $this->goals;
  }

  public function setGoals(?int $goals): self {
    $this->goals = $goals;

    return $this;
  }

  public function getAvgGoals(): ?float {
    return $this->avgGoals;
  }

  public function setAvgGoals(?float $avgGoals): self {
    $this->avgGoals = $avgGoals;

    return $this;
  }

  public function getAgeGate(): ?bool {
    return $this->ageGate;
  }

  public function setAgeGate(bool $ageGate): self {
    $this->ageGate = $ageGate;

    return $this;
  }

  public function getIsRegistered(): ?bool {
    return $this->isRegistered;
  }

  public function setIsRegistered(bool $isRegistered): self {
    $this->isRegistered = $isRegistered;

    return $this;
  }

  public function getDateOfBirth(): ?\DateTimeInterface {
    return $this->dateOfBirth;
  }

  public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self {
    $this->dateOfBirth = $dateOfBirth;

    return $this;
  }

  public function getIsCheat(): ?bool {
    return $this->isCheat;
  }

  public function setIsCheat(?bool $isCheat): self {
    $this->isCheat = $isCheat;

    return $this;
  }

  public function getToken(): bool {
    return $this->token;
  }

  public function setToken(bool $token): void {
    $this->token = $token;
  }

  public function getFbLogin(): bool {
    return $this->fbLogin;
  }

  public function setFbLogin(bool $fbLogin): void {
    $this->fbLogin = $fbLogin;
  }

  public function getGoogleLogin(): bool {
    return $this->googleLogin;
  }

  public function setGoogleLogin(bool $googleLogin): void {
    $this->googleLogin = $googleLogin;
  }

  public function getCreated(): ?\DateTimeInterface {
    return $this->created;
  }

  public function setCreated(\DateTimeInterface $created): self {
    $this->created = $created;

    return $this;
  }

  public function getUpdated(): ?\DateTimeInterface {
    return $this->updated;
  }

  public function setUpdated(\DateTimeInterface $updated): self {
    $this->updated = $updated;

    return $this;
  }

  public function getBanReason(): ?string {
    return $this->banReason;
  }

  public function setBanReason(?string $banReason): self {
    $this->banReason = $banReason;

    return $this;
  }

  public function getTime(): ?int {
    return $this->time;
  }

  public function setTime(?int $time): self {
    $this->time = $time;

    return $this;
  }

  public function getZeroPlayer(): ?bool {
    return $this->zeroPlayer;
  }

  public function setZeroPlayer(?bool $zeroPlayer): self {
    $this->zeroPlayer = $zeroPlayer;

    return $this;
  }

}
