<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\PlayerGoal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerGoal|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerGoal|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerGoal[]    findAll()
 * @method PlayerGoal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerGoalRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, PlayerGoal::class);
  }

  public function checkKicks(int $playerId, int $gameId): ?string {
    try {
      return $this->createQueryBuilder('pg')
        ->select('count(pg.id)')
        ->andWhere('pg.player = :playerId')->setParameter(':playerId', $playerId)
        ->andWhere('pg.game = :gameId')->setParameter(':gameId', $gameId)
        ->getQuery()
        ->getSingleScalarResult();
    } catch (\Exception $e) {
      return null;
    }
  }

  // /**
  //  * @return PlayerGoal[] Returns an array of PlayerGoal objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('p')
          ->andWhere('p.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('p.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?PlayerGoal
  {
      return $this->createQueryBuilder('p')
          ->andWhere('p.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
