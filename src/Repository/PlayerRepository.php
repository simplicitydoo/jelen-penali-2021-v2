<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Player|null find($id, $lockMode = null, $lockVersion = null)
 * @method Player|null findOneBy(array $criteria, array $orderBy = null)
 * @method Player[]    findAll()
 * @method Player[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, Player::class);
  }

  public function create(\DateTime $birthday): Player {
    $em = $this->getEntityManager();

    $player = new Player();
    $player->setDateOfBirth($birthday);
    $player->setAgeGate(true);

    $em->persist($player);
    $em->flush();

    return $player;
  }

  public function addData(array $playerData, int $playerId): Player {
    $em = $this->getEntityManager();

    $phone = $res = preg_replace("/[^0-9]/", "", $playerData['phone'] );

    $player = $em->getRepository(Player::class)->find($playerId);
    $player->setFirstName($playerData['firstName']);
    $player->setLastName($playerData['lastName']);
    $player->setDateOfBirth($playerData['birthday']);
    $player->setPhone($phone);
    $player->setAgeGate($playerData['ageGate']);
    $player->setCity($playerData['city']);
    $player->setAddress($playerData['address']);
    $player->setEmail($playerData['email']);
    $player->setIsRegistered(true);

    $em->flush();

    return $player;
  }

  public function getByGoals() {
    return $this->createQueryBuilder('p')
      ->andWhere('p.ageGate = 1')
      ->andWhere('p.goals is not null')
      ->addOrderBy('p.goals', 'DESC')
      ->addOrderBy('p.avgGoals', 'DESC')
      ->addOrderBy('p.time')
      ->getQuery();
  }

  public function saveAverageGoals(int $playerId): void {
    $em = $this->getEntityManager();
    $avgGoals = floatval($em->getRepository(Game::class)->getAvgGoals($playerId));

    if ($avgGoals != 0) {
      $player = $em->getRepository(Player::class)->find($playerId);
      $player->setAvgGoals($avgGoals);

      $em->flush();
    }

  }

  public function addFbData(array $playerData, int $playerId): Player {
    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);

    $player->setFirstName($playerData['fname']);
    $player->setLastName($playerData['lname']);
    $player->setEmail($playerData['email']);
    $player->setIsRegistered(true);
    $player->setFbLogin(true);

    $em->flush();

    return $player;
  }

  public function addGoogleData(array $playerData, int $playerId): Player {
    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);

    $player->setFirstName($playerData['fname']);
    $player->setLastName($playerData['lname']);
    $player->setEmail($playerData['email']);
    $player->setIsRegistered(true);
    $player->setGoogleLogin(true);

    $em->flush();

    return $player;
  }

  public function addPhone(Player $player, string $phone): void {
    $em = $this->getEntityManager();

    $player->setPhone($phone);
    $em->flush();
  }

  public function setCheater(int $playerId, string $banReason): void {
    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);
    $player->setIsCheat(true);
    $player->setBanReason($banReason);

    $em->flush();
  }

  public function setValid(int $playerId): void {
    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);

    if (is_null($player->getIsCheat())) {
      $player->setIsCheat(false);
      $em->flush();
    }

  }

//  public function getPlace(Player $player) {
//    $em = $this->getEntityManager();
//
//    try {
//      $qb = $em->createQueryBuilder();
//      $qb
//        ->select('count(p.id)')
//        ->from('App:Player', 'p')
//        ->andWhere('p.score >= :score')->setParameter('score', $player->getScore())
//        ->andWhere('p.isCheat = 0');
//
//      return $qb->getQuery()->getSingleScalarResult();
//    } catch (\Doctrine\ORM\NoResultException $e) {
//      return null;
//    }
//  }
//
  public function getPlayersExport() {
    return $this->createQueryBuilder('p')
      ->innerJoin('App:Game', 'g', Join::WITH, 'g.player = p')
      ->select('p.id as playerId, p.firstName, p.lastName, p.address, p.phone, p.email, p.city, p.goals, p.avgGoals, COUNT(g.id) AS played_games, p.isCheat, p.banReason, p.zeroPlayer')
      ->andWhere('p.ageGate = 1')
      ->andWhere('p.goals is not null')
//      ->andWhere('p.isCheat = 1')
      ->groupBy('p')
      ->orderBy('p.goals', 'DESC')
      ->getQuery()
      ->getResult();
  }

  public function getKickCheaters() { // those that didn't get caught by time difference
    return $this->createQueryBuilder('p')
      ->innerJoin('App:Game', 'g', Join::WITH, 'g.player = p')
      ->innerJoin('App:PlayerGoal', 'pg', Join::WITH, 'pg.game = g')
      ->select('p.id AS playerId, g.id AS gameId, p.firstName, p.lastName, p.address, p.phone, p.email, p.city, COUNT(pg.id) AS num_of_kicks')
      ->andHaving('num_of_kicks > 15')
      ->addGroupBy('p')
      ->addGroupBy('g')
      ->orderBy('num_of_kicks', 'DESC')
      ->getQuery()
      ->getResult();
  }

  public function checkTimes() {
    return $this->createQueryBuilder('p')
      ->innerJoin('App:Game', 'g', Join::WITH, 'g.player = p')
      ->select('p.id AS playerId, p.firstName, p.lastName, p.address, p.phone, p.email, p.city, p.goals, p.avgGoals, COUNT(g.id) AS played_games, g.serverStartTime, g.serverEndTime')
      ->andWhere('p.ageGate = 1')
      ->andWhere('p.isCheat = 1')
      ->andWhere('p.goals is not null')
      ->andWhere('g.score is not null')
      ->andWhere('g.isValid is null')
//      ->andWhere('g.serverEndTime is null')
      ->groupBy('p')
      ->addGroupBy('g')
      ->addOrderBy('p.goals', 'DESC')
      ->addOrderBy('p.avgGoals', 'DESC')
      ->getQuery()
      ->getResult();
  }

  public function getAllCheaters() {
    return $this->createQueryBuilder('p')
      ->innerJoin('App:Game', 'g', Join::WITH, 'g.player = p')
      ->innerJoin('App:PlayerGoal', 'pg', Join::WITH, 'pg.game = g')
      ->select('p.id AS playerId, p.firstName, p.lastName, p.address, p.phone, p.email, p.city, p.banReason')
      ->andWhere('p.isCheat = true')
      ->addGroupBy('p')
      ->getQuery()
      ->getResult();
  }

  public function timeForAllGames(int $playerId, int $time): void {
    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);

    if (is_null($player->getTime())) {
      $player->setTime($time);
    } else {
      $newTime = $time + $player->getTime();
      $player->setTime($newTime);
    }

    $em->flush();
  }

  public function addGameTime(int $playerId, int $gameId): void {
    $em = $this->getEntityManager();

    $game = $em->getRepository(Game::class)->find($gameId);

    $d1 = $game->getServerStartTime();
    $d2 = $game->getServerEndTime();

    $interval = $d1->diff($d2);
    $secondDiff = $interval->s;
    $time = $secondDiff;

    if ($interval->i > 0) {
      $time += $interval->i * 60;
    }

    $this->timeForAllGames($playerId, $time);
  }

  public function cheaterTime() {
    $em = $this->getEntityManager();

    $players = $em->getRepository(Player::class)->findBy(['isCheat' => true]);

    foreach ($players as $player) {
      $player->setTime(0);
    }

    $em->flush();
  }

  public function backFromTheDead(int $playerId, $numOfGoals) {

    $em = $this->getEntityManager();

    $player = $em->getRepository(Player::class)->find($playerId);
    $player->setGoals($numOfGoals);
    $player->setZeroPlayer(true);

    $em->flush();

  }

  // /**
  //  * @return Player[] Returns an array of Player objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('p')
          ->andWhere('p.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('p.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?Player
  {
      return $this->createQueryBuilder('p')
          ->andWhere('p.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
