<?php

namespace App\Repository;

use App\Classes\Helper;
use App\Entity\Code;
use App\Entity\Game;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, Game::class);
  }

  public function create(Code $code, int $playerId = null): Game {
    $em = $this->getEntityManager();

    $game = new Game();
    $game->setCode($code);
    if(!empty($playerId)) {
      $player = $em->getRepository('App:Player')->find($playerId);
      $game->setPlayer($player);
    }
    $game->setCodeEntered(new \DateTime());
//    $game->setIsSpam(0);
    $game->setHashCode(Helper::getRandomString(32));
    $em->persist($game);
    $em->flush();

    return $game;
  }

  public function addPlayer(int $gameId, int $playerId) {
    $em = $this->getEntityManager();

    if($playerId && $gameId) {
      $player = $em->getRepository('App:Player')->findOneBy(['id' => $playerId]);
      $game = $em->getRepository('App:Game')->findOneBy(['id' => $gameId]);
      $game->setPlayer($player);
    }

    $em->flush();
  }

  public function getAvgGoals(int $playerId) {
    try {
      return $this->createQueryBuilder('g')
        ->select('AVG(g.goals)')
        ->andWhere('g.player = :playerId')->setParameter(':playerId', $playerId)
        ->getQuery()
        ->getSingleScalarResult();
    } catch (\Exception $e) {
      return null;
    }
  }

  public function setInvalid(int $gameId): void {
    $em = $this->getEntityManager();

    $game = $em->getRepository(Game::class)->find($gameId);
    $player = $game->getPlayer();

    if ($player->getIsCheat() == true) {
      $game->setIsValid(false);
      $em->flush();
    }

  }

  public function calculateGames(int $playerId): void {
    $em = $this->getEntityManager();

    $games = $em->getRepository(Game::class)->findBy(['player' => $playerId]);
    $goals = 0;
    $avgGoals = 0;

    foreach ($games as $game) {

      $goals += $game->getGoals();
    }

    if ($goals != 0) {
      $avgGoals = $goals / count($games);
    }

    $player = $em->getRepository(Player::class)->find($playerId);
    $player->setGoals($goals);
    $player->setAvgGoals($avgGoals);

    try {
      $em->flush();
    } catch (\Exception $e) {
    }

  }


  // /**
  //  * @return Code[] Returns an array of Code objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('c')
          ->andWhere('c.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('c.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?Code
  {
      return $this->createQueryBuilder('c')
          ->andWhere('c.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
