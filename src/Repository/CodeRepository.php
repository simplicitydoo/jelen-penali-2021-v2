<?php

namespace App\Repository;

use App\Classes\Receipt;
use App\Entity\Code;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Code|null find($id, $lockMode = null, $lockVersion = null)
 * @method Code|null findOneBy(array $criteria, array $orderBy = null)
 * @method Code[]    findAll()
 * @method Code[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CodeRepository extends ServiceEntityRepository {
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, Code::class);
  }

  public function save(string $code, int $playerId, int $check): Code {

    $em = $this->getEntityManager();

    $codeNew = new Code();
    if ($playerId) {
      $player = $em->getRepository('App:Player')->findOneBy(['id' => $playerId]);
      $codeNew->setPlayer($player);
      $codeNew->setUsed(true);
    } else {
      $codeNew->setUsed(false);
    }

    $codeNew->setCode($code);

    if ($code == "#56321454") {
      $codeNew->setIsTest(true);
    } else {
      $codeNew->setIsTest(false);
    }

    if ($check == 1) {
      $codeNew->setVerified(true);
    } else {
      $codeNew->setVerified(false);
    }

    $em->persist($codeNew);
    $em->flush();

    return $codeNew;
  }

  public function addPlayer(Player $player, int $codeId) {
    $em = $this->getEntityManager();
    $code = $em->getRepository('App:Code')->findOneBy(['id' => $codeId]);

    $code->setPlayer($player);
    $code->setUsed(1);

    try {
      $em->flush();
    } catch (\Exception $e) {
      dump($e->getMessage());
      die();
    }
  }

  public function cifra1(string $osnova) {
    $cifra1 = 0;
    $br = 0;
    for ($i = 0; $i < 13; $i++) {
      $br = $br + 1;
      if ($br % 2 == 0) {
        $cifra1 = $cifra1 + intval(substr($osnova, $i, 1));
      } else {
        $cifra1 = $cifra1 + intval(substr($osnova, $i, 1)) * 3;
      }
    }

    $cifra1 = (10 - $cifra1 % 10) % 10;

    return $cifra1;
  }

  public function cifra2(string $osnova) {
    $cifra2 = 0;
    $br = 0;
    for ($i = 0; $i < 14; $i++) {
      $br = $br + 1;
      if ($br % 2 == 0) {
        $cifra2 = $cifra2 + intval(substr($osnova, $i, 1)) * 3;
      } else {
        $cifra2 = $cifra2 + intval(substr($osnova, $i, 1));
      }
    }
    $cifra2 = (10 - $cifra2 % 10) % 10;

    return $cifra2;
  }


  //  poslovnica | objekat | kasa | dan u mesecu | kontrola | broj racuna
  //kod = 4 012 07 05 28 0030
  //kod = 40120705280030
  //cifra1 = 2
  //cifra2 = 8
  public function checkCode(string $code): int {
    if ($code == '#56321454') {
      return 1;
    }

    $check = 0;
    $osnova = substr($code, 0, 1) . substr($code, 1, 3) . substr($code, 4, 2) . substr($code, 6, 2) . substr($code, 10, 4);

    $cifra1 = $this->cifra1($osnova);
    $cifra2 = $this->cifra2($osnova . strval($cifra1));

    $em = $this->getEntityManager();
    $codeObject = $em->getRepository('App:Code')->findOneBy(['code' => $code]);

    if ($codeObject) {
      $check = -1;
    }


    if (strlen($code) == 14) {
      if (!$codeObject) {
        if ($cifra1 == intval(substr($code, 8, 1)) && $cifra2 == intval(substr($code, 9, 1))) {
          $check = 1;
        }
      }
    }

    return $check;
  }

  // /**
  //  * @return Code[] Returns an array of Code objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('c')
          ->andWhere('c.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('c.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?Code
  {
      return $this->createQueryBuilder('c')
          ->andWhere('c.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
