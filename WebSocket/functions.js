var db = require('./db');

module.exports = {
    gameStartTime: async function(code, token) {
        try {
            const check = await db.query("SELECT COUNT(*) AS checkGame FROM games WHERE hash_code = ? AND server_start_time IS NULL", [code]);
            if (parseInt(check[0].checkGame) > 0) {
                await db.query("UPDATE games SET token = ?, server_start_time = NOW(), updated = NOW() WHERE hash_code = ?", [token, code]);
                console.log("Database start time updated");
            } else {
                console.log('No game!');
            }
        } catch (err) {
            console.log(err);
            console.log('line', 15)
        }
    },

    playerGoalUpdate: async function(goal, code, token) {

        try {
            var game = await db.query("SELECT id, fk_code_id, fk_player_id FROM games WHERE hash_code = ?", [code]);

            var values = [
                game[0].fk_player_id,
                game[0].id,
                goal,
                token
            ];

            await db.query ("INSERT INTO player_goals (fk_player_id, fk_game_id, goal, token, created) VALUES (?,?,?,?,now())", values);
            console.log('Player move updated');

        } catch (err) {
            console.log(err, 'line 61');
        }

    },
    gameEndTime: async function(code, score, token) {

        try {
            var game = await db.query("SELECT id, fk_player_id, fk_code_id FROM games WHERE hash_code = ?", [code]);

            if (typeof game[0] != 'undefined' && typeof game[0].id != 'undefined' && game.length > 0) {
                var data = await db.query("SELECT COUNT(id) AS goals FROM player_goals WHERE goal = true AND fk_game_id = ?", [game[0].id]);

                if (typeof data[0] != 'undefined') {
                    await db.query("UPDATE games SET score = ?, goals = ?, token = ?,  server_end_time = now(), updated = now() WHERE hash_code = ?", [score, data[0].goals, token, code]);
                    await db.query("UPDATE players SET fk_code_id = ?, goals = goals + ?, token = ?, updated = now() WHERE id = ? AND goals IS NOT NULL", [game[0].fk_code_id, data[0].goals, token, game[0].fk_player_id]);
                    await db.query("UPDATE players SET fk_code_id = ?, goals = ?, token = ?, updated = now() WHERE id = ? AND goals IS NULL", [game[0].fk_code_id, data[0].goals, token, game[0].fk_player_id]);
                }
            }

            console.log('Database end game data updated');

        } catch (err) {
            console.log(err);
            console.log('line:', 33)
        }
    },
}