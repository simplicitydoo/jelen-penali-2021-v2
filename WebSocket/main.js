// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";

require('../assets/node_modules/dotenv').config({path: '../.env.' + process.env.NODE_ENV})

var games = new Map();
var token;
var hash;
var wsServer;
var jwt = require('../assets/node_modules/jsonwebtoken');

if (process.env.NODE_ENV === 'local') {
    wsServer = require('./server');
} else {
    wsServer = require('./server-ssl');
}

var func = require('./functions')

wsServer.on('request', function (request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

    var connection = request.accept(null, request.origin);

    console.log((new Date()) + ' Connection accepted.');

    connection.on('message', function (message) {
        var msg = message.utf8Data;

        var type = msg.substring(0, 5);

        if (type === 'token') {
            var encodedToken = msg.substring(5);
            jwt.verify(encodedToken, process.env.SECRET_KEY,{ algorithms: ['HS512'] }, function (err, decodedToken) {
                if (!err && decodedToken.playerId) {
                    token = true;
                } else {
                    token = false;
                }
            })
        }

        if (type === 'hash-') {
            hash = msg.substring(5);
            games.set(request.key, {hash: hash});
        }

        if (type === 'start' && games.has(request.key)) {
            var game = games.get(request.key);
            func.gameStartTime(game.hash, token);
        }

        if (type === 'end--' && games.has(request.key)) {
            var game = games.get(request.key);
            var score = msg.substring(5);
            func.gameEndTime(game.hash, parseInt(score), token);
        }

        if (type === 'goal-') {
            func.playerGoalUpdate(true, hash, token);
        } else if(type === 'save-') {
            func.playerGoalUpdate(false, hash, token);
        } else if (type === 'out--') {
            func.playerGoalUpdate(false, hash, token);
        }

    });

    connection.on('close', function (connection) {
        console.log((new Date()) + " Peer "
            + connection.remoteAddress + " disconnected.");

    });

});